package com.example.cide;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CideApplication {

	public static void main(String[] args) {
		SpringApplication.run(CideApplication.class, args);
	}

}
