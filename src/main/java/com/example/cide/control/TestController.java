package com.example.cide.control;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * author: ZGF
 * 06-2020/6/2 : 14:58
 * context : 测试代码
 */

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/")
    public String test(){
        return "Hello World";
    }
}
